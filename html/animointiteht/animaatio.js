var canvas = document.getElementById("kanvaasi");
var ctx = canvas.getContext("2d");

//muutettavat
var rengasKoko = 23;
var x = 1;
var y = 250;

//loop
function loopersson() {
    ctx.clearRect(0, 0, 500, 500);

    //takarengas
    ctx.beginPath();
    ctx.arc(x+85, y+230, rengasKoko, 0, 2 * Math.PI);
    ctx.fillStyle = "white";
    ctx.fill();
    ctx.beginPath();
    ctx.arc(x+85, y+230, rengasKoko, 0, 2 * Math.PI);
    ctx.strokeStyle = "black";
    ctx.lineWidth = 8;
    ctx.stroke();
    
    //eturengas
    ctx.beginPath();
    ctx.arc(x+165, y+230, rengasKoko, 0, 2 * Math.PI);
    ctx.fillStyle = "white";
    ctx.fill();
    ctx.beginPath();
    ctx.arc(x+165, y+230, rengasKoko, 0, 2 * Math.PI);
    ctx.strokeStyle = "black";
    ctx.lineWidth = 8;
    ctx.stroke();
    
    //auto
    ctx.fillStyle = "blue";
    ctx.fillRect(x+50, y+150, 150, 70);

    //autokatto
    ctx.fillStyle = "blue";
    ctx.fillRect(x+60, y+140, 50, 50);
    ctx.fillStyle = "blue";
    ctx.fillRect(x+140, y+140, 50, 50);

    //liike
    x += 5
    if (x > 650) x = -300;
}

setInterval(loopersson, 10);