const canvas = document.getElementById('kanvaasi');
const ctx = canvas.getContext('2d');
let raf;
let running = false;

//nappitunnistus
let inputStates = {}
window.addEventListener('keydown', function(event) {
    console.log(event)
    if (event.key == "ArrowRight") {
        inputStates.right = true;
    }
});
window.addEventListener('keyup', (event)=> {
    console.log(event);
    if (event.key == "ArrowRight") {
        inputStates.right = false;
    }
});
window.addEventListener('keydown', function(event) {
    console.log(event)
    if (event.key == "ArrowLeft") {
        inputStates.left = true;
    }
});
window.addEventListener('keyup', (event)=> {
    console.log(event);
    if (event.key == "ArrowLeft") {
        inputStates.left = false;
    }
});

//pallon visuaalit ja nopeus
const ball = {
  x: 100,
  y: 100,
  vx: 5,
  vy: 2,
  radius: 25,
  color: 'blue',
  draw() {
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fillStyle = this.color;
    ctx.fill();
  }
};

//mailan visuaalit ja nopeus
const maila = {
    x: 250,
    y: 385,
    vx: 15,
    leveys: 100,
    korkeus: 15,
    color: 'red',
    draw() {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x, this.y, this.leveys, this.korkeus);
    }
};

//peli
function draw() {
  ctx.clearRect(0,0, canvas.width, canvas.height);
  ball.draw();
  ball.x += ball.vx;
  ball.y += ball.vy;
  maila.draw();

    //pallon törmäys
    if (ball.y + ball.radius > canvas.height || ball.y - ball.radius < 0) {
    ball.vy = -ball.vy;
    }
    if (ball.x + ball.radius > canvas.width || ball.x - ball.radius < 0) {
    ball.vx = -ball.vx;
    }

//mailan liike
        if (inputStates.right) {
        maila.x += maila.vx;
        }
        if (inputStates.left) {
        maila.x -= maila.vx;
        }
    canvas.addEventListener('mousemove', (e) => {
        if (!running) {
            maila.x = e.clientX - 60;
            maila.draw();
            }
          });

//mailan törmäys seinä
    if (maila.x < 0) {
        maila.x = 0;
    }
    if (maila.x + maila.leveys > canvas.width) {
        maila.x = canvas.width - maila.leveys;
    }
    
//mailan törmäys pallo
    var testX=ball.x;
    var testY=ball.y;
    
    if (testX < maila.x) testX=maila.x;
    else if (testX > (maila.x+maila.leveys)) testX=(maila.x+maila.leveys)
    if (testY < maila.y) testY=maila.y;
    else if (testY > (maila.y+maila.korkeus)) testY=(maila.y+maila.korkeus)
    
    var distX = ball.x - testX;
    var distY = ball.y - testY;
    var dista = Math.sqrt((distX*distX)+(distY*distY))
        
    if (dista <= ball.radius) {
        if (ball.x >= maila.x && ball.x <= (maila.x+maila.leveys)) {
        ball.vy *= -1;
        }
    
    else if (ball.y >= maila.y && ball.y <= (maila.y+maila.korkeus)) {
        ball.vx *= -1;
        }
    }

//loppu
  raf = window.requestAnimationFrame(draw);
}
draw();
ball.draw();